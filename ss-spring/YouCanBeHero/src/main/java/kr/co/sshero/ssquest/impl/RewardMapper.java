package kr.co.sshero.ssquest.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import kr.co.sshero.ssquest.SSQuestVO;

public class RewardMapper implements RowMapper<SSQuestVO> {
	@Override
	public SSQuestVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		SSQuestVO vo = new SSQuestVO();
		vo.setSeq(rs.getInt("SEQ"));
		vo.setRssid(rs.getString("RSSID"));
		vo.setQcoin(rs.getInt("QCOIN"));
		vo.setQexp(rs.getInt("QEXP"));
		
		return vo;
	}

}
