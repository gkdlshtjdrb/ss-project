package kr.co.sshero.quest;

import java.util.List;

import kr.co.sshero.user.UserVO;

public interface QuestService {
	
	void insertQuest(QuestVO vo);

	void updateQuest(QuestVO vo);

	void deleteQuest(QuestVO vo);

	QuestVO getQuest(QuestVO vo);
	
	List<QuestVO> getDdayQuestList(UserVO uvo);
	List<QuestVO> getDdayQuestList();
	
	List<QuestVO> getWeekQuestList();
	List<QuestVO> getWeekQuestList(UserVO uvo);
	
	List<QuestVO> getSubmitList(QuestVO vo);

}
