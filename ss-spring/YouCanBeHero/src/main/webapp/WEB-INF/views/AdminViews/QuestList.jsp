<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <jsp:useBean id="now" class="java.util.Date" />
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <link href="<c:url value="/resources/semantic/semantic.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/semantic/semantic.js" />"></script>
    <title>Document</title>


</head>
<body>
    <div class="ui secondary pointing menu">
        <a name = "week" class="item" href="getCustomWeekQuestList.do">
          	주간퀘스트
        </a>
        <a name = "dday" class="item" href="getCustomDdayQuestList.do">
          	일일퀘스트
        </a>
        <a class="item">
          My Team List
        </a>
        <a class="ui item" href = "insertQuest.do">
            Insert
          </a>
          <a class="ui item">
            Logout
          </a>
        
      </div>
      
      <div class="ui segment">
        <div class="ui relaxed divided list">
        <c:forEach items="${questList }" var="quest">
            <div class="item">
              <i class="large github middle aligned icon"></i>
              <div class="content">
                <a class="header" href = "getQuest.do?qid=${quest.qid }" data-content="${quest.qcontext }">${quest.qname }</a>
                <div class="ui label right floated content">
                  		<a href = "questSubmitList.do?qid=${quest.qid }">퀘스트 승인</a>
                </div>
                <div class="ui label right floated content">
                  		경험치
                  <div class="detail">${quest.qexp }</div>
                </div>
                <div class="ui label right floated content">
                 		 코인
                  <div class="detail">${quest.qcoin }</div>
                </div>
				<fmt:formatDate value="${now}" pattern="yyyy.MM.dd" var="nowDate" />             <%-- 오늘날짜 --%>
				<fmt:formatDate value="${quest.startdate }" pattern="yyyy.MM.dd" var="startDate"/>     <%-- 시작날짜 --%>
				<fmt:formatDate value="${quest.enddate }" pattern="yyyy.MM.dd" var="endDate"/>        <%-- 마감날짜 --%>
				<c:if test="${endDate < nowDate}">
    				<div class="ui label right floated content">
                  		<a href = "deleteQuest.do?qid=${quest.qid }" onclick="return confirm('퀘스트 승인을 확인하셨습니까?');">기간 만료 삭제 가능</a>
                	</div>
				</c:if>
                <div class="description" name = "${quest.enddate }">기간 ${startDate} == ${endDate }</div>
              </div>
            </div>
         </c:forEach>
          </div>
      </div>
      <script>
	var type = ${mapping}
	if( type == 0){
		$("a[name= week]").addClass("active");
	}
	else if(type == 1) {
		$("a[name= dday]").addClass("active");
	}
	
	$('.activating.element').popup();
    $('.header').popup({inline: true});
</script>

</body>

</html>
