<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
        <link href="<c:url value="/resources/semantic/semantic.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/semantic/semantic.js" />"></script>
    
    <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
    <title>Document</title>


</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="eight wide column">
    
      <div class="ui large card ui vertical masthead center aligned segment">
        <div class="image">
          <img src="<c:url value = "/resources/images/zed.png"/>">
        </div>
        <div class="ui content">
          <a class="header">${user.name}</a>
          <div class="meta">
            <span class="date">LV ${user.lv}</span>
          </div>
          <div class="description">
            	등급
          </div>
        </div>
        <div class="ui indicating progress" id="exp">
          <div class="bar"></div>
          <div class="label">현재 경험치</div>
        </div>
      </div>

    <div class="ui message">
      <a href="getDdayQuestList.do">오늘의 퀘스트 확인</a>
    </div>
  </div>
</div>
<script>
  $('#exp').progress({
  percent: 80
});
</script>
</body>

</html>
