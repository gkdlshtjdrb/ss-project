package kr.co.sshero.quest.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.sshero.quest.QuestService;
import kr.co.sshero.quest.QuestVO;
import kr.co.sshero.ssquest.SSQuestVO;
import kr.co.sshero.ssquest.impl.SSQuestDAO;
import kr.co.sshero.user.UserVO;

@Service("questService")
public class QuestServiceImpl implements QuestService {

	@Autowired
	private QuestDAO questDAO;
	
	@Autowired
	private SSQuestDAO SSquestDAO;
	

	public List<QuestVO> getDdayQuestList() {
		// TODO Auto-generated method stub		
		return questDAO.getDdayQuestList();
	}
	
	@Override
	public List<QuestVO> getWeekQuestList() {
		// TODO Auto-generated method stub
		return questDAO.getWeekQuestList();
	}
	
	
	@Override
	public void insertQuest(QuestVO vo) {
		DateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date tempDate = sdFormat.parse(vo.getStartdateString());
			vo.setStartdate(tempDate);
			tempDate = sdFormat.parse(vo.getEnddateString());
			vo.setEnddate(tempDate);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		questDAO.insertQuset(vo);
	}

	@Override
	public void updateQuest(QuestVO vo) {
		DateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date tempDate = sdFormat.parse(vo.getStartdateString());
			vo.setStartdate(tempDate);
			tempDate = sdFormat.parse(vo.getEnddateString());
			vo.setEnddate(tempDate);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}

		
		questDAO.updateQuest(vo);

	}

	@Override
	public void deleteQuest(QuestVO vo) {
		questDAO.deleteQuest(vo);

	}

	@Override
	public QuestVO getQuest(QuestVO vo) {
		return questDAO.getQuest(vo);
	}
	
	@Override
	public List<QuestVO> getSubmitList(QuestVO vo) {
		System.out.println("qid 확인"+vo.getQid());
		
		return questDAO.getSubmitList(vo);
	}
	
	//======================ss이 사용하는 기능=====================================
	
	@Override
	public List<QuestVO> getDdayQuestList(UserVO uvo) {
		// TODO Auto-generated method stub
		//����Ʈ ����Ʈ
		List<QuestVO> qlist = questDAO.getDdayQuestList();
		QuestVO vo = new QuestVO();
		int check;
		//�ش� qid�� ���� ��¥�� �񱳸� �ϱ� ���ؼ���
		//qid�� userid�� �ʿ���.
		SSQuestVO svo = new SSQuestVO();
		svo.setRssid(uvo.getSsid());
		System.out.println("qlistSize ="+ qlist.size());
		
		
		
		for(int i = 0; i<qlist.size(); i++) {

			vo = qlist.get(i);
			svo.setRqid(vo.getQid());
			check = SSquestDAO.getSysdatecheck(svo);
			System.out.println("svo.getRqid =="+svo.getRqid()+" check "+check);
			//check�� 1�̿��� ��� ���� ��¥�� ��ġ�� ��
			if(check == 1) {
				qlist.remove(i);
				i--;
			}
		
		}


		return qlist;
	}
	

	@Override
	public List<QuestVO> getWeekQuestList(UserVO uvo) {
		// TODO Auto-generated method stub	
		//����Ʈ ����Ʈ
		List<QuestVO> qlist = questDAO.getWeekQuestList();
		QuestVO vo = new QuestVO();
		int check;
		//�ش� qid�� ���� ��¥�� �񱳸� �ϱ� ���ؼ���
		//qid�� userid�� �ʿ���.
		SSQuestVO svo = new SSQuestVO();
		svo.setRssid(uvo.getSsid());
		svo.setStartdate(startDayOfWeek());
		svo.setEnddate(endDayOfWeek());
		System.out.println("qlistSize ="+ qlist.size());
				
				
				
		for(int i = 0; i<qlist.size(); i++) {

			vo = qlist.get(i);
			svo.setRqid(vo.getQid());
			check = SSquestDAO.getDayOfWeekcheck(svo);
			System.out.println("svo.getRqid =="+svo.getRqid()+" check "+check);
			//check�� 1�̿��� ��� ���� ��¥�� ��ġ�� ��
			if(check >= 1) {
				qlist.remove(i);
				i--;
			}
				
		}


				

		return qlist;
	}
	
	
	
	public String startDayOfWeek() {
		Calendar cal = Calendar.getInstance();
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
//		startdate�� ���ϵǴ� ���� ������ -1�� sysdate���� ���ָ� ��.
//		enddate�� ���ϵǴ� 9 - ���� �������� sysdate�� �����ָ� ��.
		cal.setTime(new Date());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("���� ��¥" + df.format(cal.getTime()));
 
        
		cal.add(Calendar.DATE, -(dayOfWeek-1));
		System.out.println("after: " + df.format(cal.getTime()));

		return df.format(cal.getTime());
	}
	
	public String endDayOfWeek() {
		Calendar cal = Calendar.getInstance();
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
//		startdate�� ���ϵǴ� ���� ������ -1�� sysdate���� ���ָ� ��.
//		enddate�� ���ϵǴ� 9 - ���� �������� sysdate�� �����ָ� ��.
		cal.setTime(new Date());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("���� ��¥" + df.format(cal.getTime()));
 
        
        cal.add(Calendar.DATE, (9-dayOfWeek));
		System.out.println("after: " + df.format(cal.getTime()));
		return df.format(cal.getTime());
	}

}
