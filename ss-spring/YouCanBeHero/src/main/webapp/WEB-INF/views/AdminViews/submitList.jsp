<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <link href="<c:url value="/resources/semantic/semantic.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/semantic/semantic.js" />"></script>
    <title>submitList</title>


</head>
<body>
    <div class="ui secondary pointing menu">
        <a class="item" href="getCustomWeekQuestList.do">
          	주간 QuestCustom
        </a>
        <a class="item" href="getCustomDdayQuestList.do">
          	일일 QuestCustom
        </a>
        <a class="item">
          My Team List
        </a>
        <a class="item">
          QuestCheck
        </a>
        <div class="right menu">
          <a class="ui item" href = "insertQuest.do">
            Insert
          </a>
          <a class="ui item">
            Logout
          </a>
        </div>
      </div>
      <div class="ui segment">
        <div class="ui relaxed divided list">
        
        <c:forEach items="${submitList }" var="submit">
            <div class="item">
              <i class="large github middle aligned icon"></i>
              <div class="content">
                <a class="header" href = "submitCheck.do?seq=${submit.seq }">${submit.rname }</a>
               
                <div class="ui label right floated content">
                 		승인 대기 중
                </div>
                <div class="description">${submit.qname}</div>
              </div>
            </div>
         </c:forEach>
          </div>
      </div>
</body>
</html>
