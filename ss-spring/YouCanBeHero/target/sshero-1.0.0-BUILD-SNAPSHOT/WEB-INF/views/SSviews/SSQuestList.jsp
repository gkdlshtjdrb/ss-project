<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <jsp:useBean id="now" class="java.util.Date" />
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <link href="<c:url value="/resources/semantic/semantic.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/semantic/semantic.js" />"></script>



</head>
<body>
    <div class="ui secondary pointing menu">
        <a name = "dday" class="item" href="getDdayQuestList.do">
          	일일 퀘스트
        </a>
        <a name = "week" class="item" href="getWeekQuestList.do">
         	주간 퀘스트
        </a>
        <a class="item" href = "myQuestList.do">
          	나의 퀘스트 현황
        </a>
        <div class="right menu">
          <a class="ui item">
            Logout
          </a>
        </div>
      </div>
      <div class="ui segment">
        <div class="ui relaxed divided list">
        
        <c:forEach items="${questList }" var="quest">
        	<fmt:formatDate value="${now}" pattern="yyyy.MM.dd" var="nowDate" />             <%-- 오늘날짜 --%>
			<fmt:formatDate value="${quest.startdate }" pattern="yyyy.MM.dd" var="startDate"/>     <%-- 시작날짜 --%>
			<fmt:formatDate value="${quest.enddate }" pattern="yyyy.MM.dd" var="endDate"/>        <%-- 마감날짜 --%>
			<c:if test="${startDate < nowDate && endDate > nowDate}">
    			<div class="item">
              		<i class="large github middle aligned icon"></i>
              		<div class="content">
                		<a class="header" href = "insertRunQuestView.do?qid=${quest.qid }" data-content="${quest.qcontext }">${quest.qname }</a>
                		<div class="ui label right floated content">
                  		경험치
                  			<div class="detail">${quest.qexp }</div>
                		</div>
                		<div class="ui label right floated content">
                 		 코인
                  			<div class="detail">${quest.qcoin }</div>
                		</div>
                		<div class="description">기간 ${quest.startdate } == ${quest.enddate }</div>
              		</div>
            	</div>
			</c:if>
            
         </c:forEach>
          </div>
      </div>
      <script>
        $('.activating.element').popup();
        $('.header').popup({inline: true});
        
        var type = ${mapping}
    	if( type == 0){
    		$("a[name= week]").addClass("active");
    	}
    	else if(type == 1) {
    		$("a[name= dday]").addClass("active");
    	}
    	
      </script>
  
</body>
</html>
