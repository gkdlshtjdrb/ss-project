package kr.co.sshero.ssquest.impl;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import kr.co.sshero.fileIO.FileIO;
import kr.co.sshero.quest.QuestVO;
import kr.co.sshero.quest.impl.QuestDAO;
import kr.co.sshero.ssquest.QuestFileVO;
import kr.co.sshero.ssquest.SSQuestService;
import kr.co.sshero.ssquest.SSQuestVO;
import kr.co.sshero.user.UserVO;

@Service("SSquestService")
public class SSQuestServiceImpl implements SSQuestService {

	private static final String UPLOAD_PATH = "D:\\programing\\fileuploadtest";
	
	@Autowired
	private SSQuestDAO SSquestDAO;
	
	@Autowired
	private QuestDAO questDAO;
	
	@Autowired
	private FileIO fileio;
	
	@Override
	public void insertSSQuest(SSQuestVO vo) {
		MultipartFile[] files = vo.getUploadFile();

		SSquestDAO.insertSSQuest(fileio.insertFile(vo, files));
	}

	@Override
	public void updateSSQuest(SSQuestVO vo) {
		MultipartFile[] files = vo.getUploadFile();
		String rcontext = vo.getRcontext();
		vo = SSquestDAO.getMyQuest(vo);
		vo.setRcontext(rcontext);
		System.out.println("vo.getrfile"+ vo.getRfile());

		String savename = "";
		if(vo.getRfile() == null) {
			SSquestDAO.updateSSQuest(fileio.insertFile(vo, files));
		}else {
			savename = vo.getRfile();
			System.out.println("savename"+ savename);
			SSquestDAO.updateSSQuest(fileio.insertFile(vo, files ,savename));
		}
		SSquestDAO.updateSSQuest(vo);
		
	}

	@Override
	public void deleteSSQuest(SSQuestVO vo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SSQuestVO getMyQuest(SSQuestVO vo) {
		
		return SSquestDAO.getMyQuest(vo);
	}

	@Override
	public List<SSQuestVO> getMyQuestList(UserVO vo) {

		return SSquestDAO.getMyQuestList(vo);
	}
	
	
	@Override
	public void questReward(SSQuestVO vo) {
		System.out.println("service �׽�Ʈ seq = " + vo.getSeq());

		vo = SSquestDAO.getQuestInfo(vo);
		System.out.println("service �׽�Ʈ id" + vo.getRssid() + "����ġ = "+vo.getQexp()+ "coin = "+ vo.getQcoin());

		SSquestDAO.questReward(vo);
		
		

		
		
		
		
	}
	
	@Override
	public void deleteFile(QuestFileVO vo) {
		// TODO Auto-generated method stub
		File file = new File(UPLOAD_PATH+"\\"+vo.getFilename());
		System.out.println(file.toString());


		SSQuestVO svo = new SSQuestVO();
		svo.setSeq(vo.getSeq());
		svo = SSquestDAO.getMyQuest(svo);
		
		String[] filepath = svo.getRfile().split(",");
		String saveRfile = "";
		for(int i = 0; i < filepath.length; i++) {
			if(filepath[i].equals(vo.getFilename())) {
			}else {
				saveRfile += filepath[i]; 
				saveRfile += ",";
			}
		}
		svo.setRfile(saveRfile);
		SSquestDAO.updateRfile(svo);

		if( file.exists() ){
			
			if(file.delete()){
				System.out.println("���ϻ��� ����"); 
			}
			else
			{
				System.out.println("���ϻ��� ����"); 
				} 
			}
		else{
				System.out.println("������ �������� �ʽ��ϴ�."); 
				} 
		
		
	}
	
	@Override
	public void rcheckUpdate(SSQuestVO vo) {
		// TODO Auto-generated method stub
		SSquestDAO.updateRcheck(vo);
	}
	


	
}
