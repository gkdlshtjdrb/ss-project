package kr.co.sshero.view.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import kr.co.sshero.user.UserService;
import kr.co.sshero.user.UserVO;

@Controller
@SessionAttributes("user")
public class LoginController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/")
	public String loginView() {
		return "main";
	}
	
	@RequestMapping("login.do")
	public String login(UserVO vo, Model model) {
		UserVO user = userService.getUser(vo);
		if(user != null) {
			model.addAttribute("user", user);
			if(1 == user.getAdmin()) {
				return("redirect:getCustomDdayQuestList.do");
			}
			else {
				return("SSviews/SSmypage");
			}
		}
		else {
			System.out.println("main.jsp �ҷ���");
			return "main";
		}

		
	}
	
	@RequestMapping("signUpView.do")
	public String signUpView() {
		return "signUp";
	}
	
	@RequestMapping("signUp.do")
	public String signUp(UserVO vo, Model model) {
		System.out.println("컨트롤에서 받은 정보"+vo.getSsid()+vo.getSsname()+vo.getSspassword());
		vo = userService.signUp(vo);
		
		if(vo.getSsid().equals("")) {
			model.addAttribute("idcheck", "아이디가 중복되었습니다.");
			return "signUp";
		}else {
			model.addAttribute("user", vo);
			return "SSviews/SSmypage";
		}
	}
}
