package kr.co.sshero.view.quest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kr.co.sshero.fileIO.FileIO;
import kr.co.sshero.quest.QuestVO;
import kr.co.sshero.quest.impl.QuestDAO;
import kr.co.sshero.ssquest.QuestFileVO;
import kr.co.sshero.ssquest.SSQuestService;
import kr.co.sshero.ssquest.SSQuestVO;
import kr.co.sshero.user.UserService;
import kr.co.sshero.user.UserVO;

@Controller
@SessionAttributes("user")
public class SSQuestController {
	
	@Autowired
	private SSQuestService SSquestService;
	
	@Autowired
	private FileIO fileio;
	
	
	
	/*
	 * ss들이 퀘스트를 입력하기 위한 ui를 보여주는 요청
	 * */
	@RequestMapping("insertSSQuestView.do")
	public String insertRunQuestView(@ModelAttribute("user") UserVO vo, @RequestParam(value = "qid") int qid, Model model) {
		SSQuestVO svo = new SSQuestVO();
		svo.setRqid(qid);
		svo.setRssid(vo.getSsid());
		model.addAttribute("ssquest", svo);
		return "SSviews/insertSSQuest";
	}
	
	/*ss들이 입력한 정보를 실제 database에 입력하는 작업*/
	@RequestMapping("insertSSQuest.do")
	public String insertRunQuest(SSQuestVO vo, @ModelAttribute("user") UserVO uvo) {
		System.out.println("ssid =" + vo.getRssid() + "qid =" + vo.getRqid() + "context" + vo.getRcontext()+"ssname"+uvo.getSsname());
		vo.setRname(uvo.getSsname());
		SSquestService.insertSSQuest(vo);
		return "redirect:getDdayQuestList.do";
	}
	
	//ss들이 각자 자기가 수행한 퀘스트를 보여주는 리스트
	@RequestMapping("myQuestList.do")
	public String myQuestList(@ModelAttribute("user") UserVO vo, Model model) {
		System.out.println("myQuestList ���� �� Ȯ�� =" + vo.getSsid());
		model.addAttribute("myQuestList",SSquestService.getMyQuestList(vo));
		return "SSviews/myQuestList";
	}
	
	//ss들이 보상받기 버튼을 클릭시 해당 퀘스트에 관련한 exp와 coin을 적용하는 작업
	@RequestMapping("questReward.do")
	public String questReward( @RequestParam(value = "seq") int seq) {
		
		SSQuestVO vo = new SSQuestVO();
		vo.setSeq(seq);
		
		SSquestService.questReward(vo);
		
		return "redirect:myQuestList.do";
		

	}
	
	//자신이 수행한 퀘스트의 상세정보를 보여주는 작업
	@RequestMapping("getMyQuest.do")
	public String getMyQuest(@RequestParam(value = "seq") int seq, Model model) {
		System.out.println("getMyQuestController = "+ seq);
		SSQuestVO vo = new SSQuestVO();
		vo.setSeq(seq);
		vo = SSquestService.getMyQuest(vo);
		model.addAttribute("myQuest", vo);

		//file불러오기
		String files = vo.getRfile();
		System.out.println("getrfile =="+files);
		List<QuestFileVO> filess = new ArrayList<>();
		if(!files.equals("")) {
			model.addAttribute("files", fileio.fileLoad(files, filess));
		}
		return "SSviews/getMyQuest";
	}
	
	//ss들이 자신이 수행한 퀘스트 수정시 등록한 파일을 삭제하기 위한 요청을 처리하는 매핑
	@RequestMapping("deleteFile.do")
	public String deleteFile(QuestFileVO vo, RedirectAttributes redirect) {
		System.out.println("사용자로부터 받은 파일 name"+ vo.getFilename()+"seq"+ vo.getSeq());

		SSquestService.deleteFile(vo);
		redirect.addAttribute("seq", vo.getSeq());
		return "redirect:getMyQuest.do";
	
	}
	
	//ss들이 자신이 수행한 퀘스트 내용을 수정하기 위해 처리하는 매핑
	@RequestMapping("updateMyquest.do")
	public String updateMyQuest(SSQuestVO vo, RedirectAttributes redirect) {
		SSquestService.updateSSQuest(vo);
		redirect.addAttribute("seq", vo.getSeq());
		return "redirect:getMyQuest.do";
	}
	
	//admin이 ss들이 수행한 퀘스트 정보를 조회하기 위해서 처리하는 매핑
		@RequestMapping("submitCheck.do")
		public String submitCheck(SSQuestVO vo, Model model) {
				
			vo = SSquestService.getMyQuest(vo);
			model.addAttribute("myQuest", vo);
			String files = vo.getRfile();
			List<QuestFileVO> filess = new ArrayList<>();
				
			if(!files.equals("")) {
				model.addAttribute("files", fileio.fileLoad(files, filess));
			}
				
			return "AdminViews/submitCheck";
		}
	
	@RequestMapping("rcheck.do")
	public String questRcheck(SSQuestVO vo, RedirectAttributes redirect) {
		SSquestService.rcheckUpdate(vo);
		redirect.addAttribute("rqid", vo.getRqid());
		
		//questSubmitList.do
		return "redirect:questSubmitList.do";
	}
}
