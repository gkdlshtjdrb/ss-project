<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
        <link href="<c:url value="/resources/semantic/semantic.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/semantic/semantic.js" />"></script>
    <title>Document</title>


</head>
<body>
<div class="ui secondary pointing menu">
        <a class="item" href="getDdayQuestList.do">
          	일일 퀘스트
        </a>
        <a class="item" href="getWeekQuestList.do">
         	주간 퀘스트
        </a>
        <a class="item" href = "myQuestList.do">
          	나의 퀘스트 현황
        </a>
        <div class="right menu">
          <a class="ui item">
            Logout
          </a>
        </div>
      </div>
<form action = "insertRunQuest.do" method = "post" enctype = "multipart/form-data">
<input name="rqid" type="hidden" value="${ssquest.rqid}"/>
<input name="rssid" type="hidden" value="${ssquest.rssid}"/>
<div class="ui form">
    <div class="field">
      <label>내용</label>
      <textarea name = "rcontext"></textarea>
    </div>
    <input type="file" name="uploadFile" id="uploadFile" multiple>
        <div id="preview"></div>
    <br>
    <button class="ui primary submit button">
      	등록
    </button>
    
  </div>

</form>    
</body>

<script type="text/javascript">
  $(document).ready(function (e){
	  //input type에 벨류 값이 변할 경우 이벤트 발생
    $("input[type='file']").change(function(e){

      //div 내용 비워주기
      $('#preview').empty();

      //해당 이벤트 발생시 넘겨받는 파일을 타겟으로 선택하여 가져온다.
      var files = e.target.files;
      
      //해당 파일을 잘 배열로 정리
      var arr =Array.prototype.slice.call(files);
      
      //업로드 가능 파일인지 체크
      for(var i=0;i<files.length;i++){
        if(!checkExtension(files[i].name,files[i].size)){
          return false;
        }
      }
      //실제 이미지를 뿌리는 함수 역할
      preview(arr);
      
      
    });//file change
    
    function checkExtension(fileName,fileSize){

      var regex = new RegExp("(.*?)\.(exe|sh|zip|alz)$");
      var maxSize = 20971520;  //20MB
      
      if(fileSize >= maxSize){
        alert('파일 사이즈 초과');
        $("input[type='file']").val("");  //파일 초기화
        return false;
      }
      
      if(regex.test(fileName)){
        alert('업로드 불가능한 파일이 있습니다.');
        $("input[type='file']").val("");  //파일 초기화
        return false;
      }
      return true;
    }
    
    function preview(arr){
      arr.forEach(function(f){
        
        //파일명이 길면 파일명...으로 처리
        var fileName = f.name;
        if(fileName.length > 10){
          fileName = fileName.substring(0,7)+"...";
        }
        
        //div에 이미지 추가
        var str = '<div style="display: inline-flex; padding: 10px;"><li>';
        str += '<span>'+fileName+'</span><br>';
        
        //이미지 파일 미리보기
        if(f.type.match('image.*')){
          var reader = new FileReader(); //파일을 읽기 위한 FileReader객체 생성
          reader.onload = function (e) { //파일 읽어들이기를 성공했을때 호출되는 이벤트 핸들러
            //str += '<button type="button" class="delBtn" value="'+f.name+'" style="background: red">x</button><br>';
            str += '<img class = "ui fluid image" src="'+e.target.result+'" title="'+f.name+'"/>';
            str += '</li></div>';
            $(str).appendTo('#preview');
          } 
          reader.readAsDataURL(f);
        }else{
          str += '<img src="/resources/img/fileImg.png" title="'+f.name+'" width=300 height=300 />';
          $(str).appendTo('#preview');
        }
      });//arr.forEach
    }
  });
</script>
</html>
