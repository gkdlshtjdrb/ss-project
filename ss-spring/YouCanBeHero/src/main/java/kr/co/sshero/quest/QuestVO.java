package kr.co.sshero.quest;

import java.util.Date;

public class QuestVO {
	private int qid;
	private String qname;
	private String qcontext;
	private Date qdate;
	private Date enddate;
	private Date startdate;
	private int weekcheck;
	private int qexp;
	private int qcoin;
	private String enddateString;
	private String startdateString;
	private String rssid;
	private int seq;
	private int deletecheck;
	private String rname;


	
	public int getQid() {
		return qid;
	}
	public void setQid(int qid) {
		this.qid = qid;
	}
	public String getQname() {
		return qname;
	}
	public void setQname(String qname) {
		this.qname = qname;
	}
	public String getQcontext() {
		return qcontext;
	}
	public void setQcontext(String qcontext) {
		this.qcontext = qcontext;
	}
	public Date getQdate() {
		return qdate;
	}
	public void setQdate(Date qdate) {
		this.qdate = qdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	
	public int getWeekcheck() {
		return weekcheck;
	}
	public void setWeekcheck(int weekcheck) {
		this.weekcheck = weekcheck;
	}
	public int getQexp() {
		return qexp;
	}
	public void setQexp(int qexp) {
		this.qexp = qexp;
	}
	public int getQcoin() {
		return qcoin;
	}
	public void setQcoin(int qcoin) {
		this.qcoin = qcoin;
	}
	public String getEnddateString() {
		return enddateString;
	}
	public void setEnddateString(String enddateString) {
		this.enddateString = enddateString;
	}
	public String getStartdateString() {
		return startdateString;
	}
	public void setStartdateString(String startdateString) {
		this.startdateString = startdateString;
	}
	public String getRssid() {
		return rssid;
	}
	public void setRssid(String rssid) {
		this.rssid = rssid;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public int getDeletecheck() {
		return deletecheck;
	}
	public void setDeletecheck(int deletecheck) {
		this.deletecheck = deletecheck;
	}
	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	
	
	
	
	
	
	
}
