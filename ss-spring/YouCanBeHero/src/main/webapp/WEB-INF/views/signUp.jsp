<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <!-- Site Properties -->
  <title>Login Example - Semantic</title>
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
 
       <link href="<c:url value="/resources/semantic/semantic.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/semantic/semantic.js" />"></script>

  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
  <script language="javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
  <script>
    function checkz() {
      var hobbyCheck = false;
      var getMail = RegExp(/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/);
      var getCheck= RegExp(/^[a-zA-Z0-9]{4,12}$/);
      var getName= RegExp(/^[가-힣]+$/);
      var fmt = RegExp(/^\d{6}[1234]\d{6}$/); //형식 설정
      var buf = new Array(13); //주민등록번호 배열
 
 
      //아이디 공백 확인
      if($("#id").val() == ""){
        alert("아이디 입력바람");
        $("#id").focus();
        return false;
      }
 
      //아이디 유효성 검사
      if(!getCheck.test($("#id").val())){
        alert("형식에 맞게 입력해주세요");
        $("#id").val("");
        $("#id").focus();
        return false;
      }

      //비밀번호 공백 확인
      if($("#password").val() == ""){
        alert("비밀번호 입력바람");
        $("#password").focus();
        return false;
      }
 
      //비밀번호
      if(!getCheck.test($("#password").val())) {
      alert("형식에 맞춰서 PW를 입력");
      $("#password").val("");
      $("#password").focus();
      return false;
      }
 
      //아이디랑 비밀번호랑 같은지
      if ($("#id").val()==($("#password").val())) {
      alert("비밀번호와 아이디는 같으면 안 됩니다.");
      $("#password").val("");
      $("#password").focus();
    }
 
      //비밀번호 똑같은지
      if($("#password").val() != ($("#cpass").val())){ 
      alert("비밀번호가 틀렸습니다.");
      $("#password").val("");
      $("#cpass").val("");
      $("#password").focus();
      return false;
     }
 
     //이름 공백 확인
      if($("#name").val() == ""){
        alert("이름을 입력해주세요");
        $("#name").focus();
        return false;
      }
 
      //이름 유효성
      if (!getName.test($("#name").val())) {
        alert("이름의 형식이 틀립니다.");
        $("#name").val("");
        $("#name").focus();
        return false;
      }
    return true;
  }
  </script>
</head>
<body>
<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <div class="content">
        회원가입
      </div>
    </h2>
    <form class="ui large form" onsubmit="return checkz()" method="post"  action="signUp.do"">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="ssid" placeholder="ID" id="id" value = "${idcheck }">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="sspassword" placeholder="비밀번호" id="password">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" placeholder="비밀번호 확인" id="cpass">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="text" name="ssname" placeholder="이름" id="name">
          </div>
        </div>
        <button class="ui fluid large teal button" type="submit">등록</button>
      </div>

    </form>

  </div>
</div>
</body>

</html>
