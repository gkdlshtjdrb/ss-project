<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <link href="<c:url value="/resources/semantic/semantic.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/semantic/semantic.js" />"></script>
    <script type="text/javascript">
      $(function(){
        $('.item').click(function(){
          $(this)
            .addClass('active')
            .closest('.ui.menu')
            .find('.item')
              .not($(this))
              .removeClass('active')
          ;
        });
      });
      </script>


</head>
<body>
    <div class="ui secondary pointing menu">
        <a class="active item" href="getDdayQuestList.do">
          	일일 퀘스트
        </a>
        <a class="item" href="getWeekQuestList.do">
         	주간 퀘스트
        </a>
        <a class="item" href = "myQuestList.do">
          	나의 퀘스트 현황
        </a>
        <div class="right menu">
          <a class="ui item">
            Logout
          </a>
        </div>
      </div>
      <div class="ui segment">
        <div class="ui relaxed divided list">
        
        <c:forEach items="${questList }" var="quest">
            <div class="item">
              <i class="large github middle aligned icon"></i>
              <div class="content">
                <a class="header" href = "insertRunQuestView.do?qid=${quest.qid }" data-content="${quest.qcontext }">${quest.qname }</a>
         
                <div class="ui label right floated content">
                  		경험치
                  <div class="detail">${quest.qexp }</div>
                </div>
                <div class="ui label right floated content">
                 		 코인
                  <div class="detail">${quest.qcoin }</div>
                </div>
                <div class="description">기간 ${quest.startdate } == ${quest.enddate }</div>
              </div>
            </div>
         </c:forEach>
          </div>
      </div>
      <script type="text/javascript" src="semantic/menu.js"></script>
      <script>
        $('.activating.element').popup();
        $('.header').popup({inline: true});
      </script>
</body>
</html>
