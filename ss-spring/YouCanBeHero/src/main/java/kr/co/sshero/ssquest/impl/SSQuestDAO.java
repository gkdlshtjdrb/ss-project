package kr.co.sshero.ssquest.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import kr.co.sshero.quest.QuestVO;
import kr.co.sshero.ssquest.QuestFileVO;
import kr.co.sshero.ssquest.SSQuestVO;
import kr.co.sshero.user.UserVO;

@Repository("SSquestDAO")
public class SSQuestDAO {
	
	@Autowired
	public JdbcTemplate jdbcTemplate;
	
	@Autowired
	private SqlSessionTemplate mybatis;
	
	private final String UPDATE_RCHECK = "update runquest set rcheck = 1 where seq = ?";

	
	public void insertSSQuest(SSQuestVO vo) {
		mybatis.update("SSQuestDAO.insertSSQuest", vo);
	}
	
	public List<SSQuestVO> getMyQuestList(UserVO vo) {
		
		return mybatis.selectList("SSQuestDAO.getMyQuestList", vo);
	}
	
	public SSQuestVO getQuestInfo(SSQuestVO vo){
		return mybatis.selectOne("SSQuestDAO.getQuestInfo", vo);
	}
	
	public void questReward(SSQuestVO vo) {
		mybatis.update("SSQuestDAO.questReward", vo);
		mybatis.update("SSQuestDAO.submitQuest", vo);
	}
	
	public SSQuestVO getMyQuest(SSQuestVO vo) {
		return mybatis.selectOne("SSQuestDAO.getMyQuest", vo);
	}
	
	public int getSysdatecheck(SSQuestVO vo) {
		return mybatis.selectOne("SSQuestDAO.getSysdatecheck", vo);
	}
	
	public int getDayOfWeekcheck(SSQuestVO vo) {
		return mybatis.selectOne("SSQuestDAO.getDayOfWeekcheck", vo);
	}
	

	public void updateRfile(SSQuestVO vo) {
		mybatis.update("SSQuestDAO.updateRfile", vo);
	}
	
	public void updateSSQuest(SSQuestVO vo) {
		mybatis.update("SSQuestDAO.updateSSQuest", vo);
	}
	
	//==========================================
	public void updateRcheck(SSQuestVO vo) {
		mybatis.update("SSQuestDAO.updateRcheck", vo);
	}
	

	
	
}
