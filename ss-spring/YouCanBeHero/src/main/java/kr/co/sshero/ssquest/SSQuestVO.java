package kr.co.sshero.ssquest;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class SSQuestVO {
	private int seq;
	private int rqid;
	private String rcontext;
	private Date rdate;
	private String rssid;
	private int rcheck;
	private int rsubmit;
	private String rfile;
	private MultipartFile[] uploadFile;
	private String qname;
	private int qexp;
	private int qcoin;
	private String rname;
	private String startdate;
	private String enddate;
	
	
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public int getRqid() {
		return rqid;
	}
	public void setRqid(int rqid) {
		this.rqid = rqid;
	}
	public String getRcontext() {
		return rcontext;
	}
	public void setRcontext(String rcontext) {
		this.rcontext = rcontext;
	}
	public Date getRdate() {
		return rdate;
	}
	public void setRdate(Date rdate) {
		this.rdate = rdate;
	}
	public String getRssid() {
		return rssid;
	}
	public void setRssid(String rssid) {
		this.rssid = rssid;
	}
	public int getRcheck() {
		return rcheck;
	}
	public void setRcheck(int rcheck) {
		this.rcheck = rcheck;
	}
	public int getRsubmit() {
		return rsubmit;
	}
	public void setRsubmit(int rsubmit) {
		this.rsubmit = rsubmit;
	}
	public String getRfile() {
		return rfile;
	}
	public void setRfile(String rfile) {
		this.rfile = rfile;
	}
	
	public MultipartFile[] getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(MultipartFile[] uploadFile) {
		this.uploadFile = uploadFile;
	}
	public String getQname() {
		return qname;
	}
	public void setQname(String qname) {
		this.qname = qname;
	}
	public int getQexp() {
		return qexp;
	}
	public void setQexp(int qexp) {
		this.qexp = qexp;
	}
	public int getQcoin() {
		return qcoin;
	}
	public void setQcoin(int qcoin) {
		this.qcoin = qcoin;
	}
	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	
	
	
	
	
	

}
