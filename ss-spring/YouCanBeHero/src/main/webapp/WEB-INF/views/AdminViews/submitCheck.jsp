<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
        <link href="<c:url value="/resources/semantic/semantic.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/semantic/semantic.js" />"></script>
    <title>Document</title>
    
    <script>
    // content, cate, index를 인수로 받아 form 태그로 전송하는 함수
    function goPage(seq ,rqid) {
      // name이 paging인 태그
      var f = document.paging;

      // form 태그의 하위 태그 값 매개 변수로 대입
      f.seq.value = seq;
      f.rqid.value = rqid;

      // input태그의 값들을 전송하는 주소
      f.action = "rcheck.do"

      // 전송 방식 : post
      f.method = "post"
      f.submit();
    };
    </script>


</head>
<body>
<div class="ui secondary pointing menu">
        <a class="item" href="getCustomWeekQuestList.do">
          	주간 QuestCustom
        </a>
        <a class="item" href="getCustomDdayQuestList.do">
          	일일 QuestCustom
        </a>
        <a class="item">
          My Team List
        </a>
        <a class="item">
          QuestCheck
        </a>
        <div class="right menu">
          <a class="ui item" href = "insertQuest.do">
            Insert
          </a>
          <a class="ui item">
            Logout
          </a>
        </div>
      </div>
<form name="paging">
    	<input type="hidden" name="seq"/>
    	<input type="hidden" name="rqid"/>
    </form>
<input name="rqid" type="hidden" value="${myQuest.rqid}"/>
<input name="rssid" type="hidden" value="${myQuest.rssid}"/>
<input name="seq" type="hidden" value="${myQuest.seq}"/>
<div class="ui form">
    <div class="field">
      <label>내용</label>
      <textarea name = "rcontext">${myQuest.rcontext}</textarea>
    </div>
    <input type="file" name="uploadFile" id="uploadFile" multiple>
        <c:forEach items="${files}" var="file">
        <a href="fileDownload.do?filepath=${file.filepath }" onclick="return confirm('다운로드 하시겠습니까?');">${file.filename} 파일 다운로드</a>
     	<br>
          <img class="ui fluid image" src="/sshero/images/${file.filepath}" />
     	
         </c:forEach>
    <br>
    <a class="ui primary submit button" href="javascript:goPage(${myQuest.seq}, ${myQuest.rqid});">
      	승인 
    </a>
    
  </div>
 
</body>
<script type="text/javascript">
  $(document).ready(function (e){
	  //input type에 벨류 값이 변할 경우 이벤트 발생
    $("input[type='file']").change(function(e){

      //div 내용 비워주기
      $('#preview').empty();

      //해당 이벤트 발생시 넘겨받는 파일을 타겟으로 선택하여 가져온다.
      var files = e.target.files;
      
      //해당 파일을 잘 배열로 정리
      var arr =Array.prototype.slice.call(files);
      
      //업로드 가능 파일인지 체크
      for(var i=0;i<files.length;i++){
        if(!checkExtension(files[i].name,files[i].size)){
          return false;
        }
      }
      //실제 이미지를 뿌리는 함수 역할
      preview(arr);
      
      
    });//file change
    
    function checkExtension(fileName,fileSize){

      var regex = new RegExp("(.*?)\.(exe|sh|zip|alz)$");
      var maxSize = 20971520;  //20MB
      
      if(fileSize >= maxSize){
        alert('파일 사이즈 초과');
        $("input[type='file']").val("");  //파일 초기화
        return false;
      }
      
      if(regex.test(fileName)){
        alert('업로드 불가능한 파일이 있습니다.');
        $("input[type='file']").val("");  //파일 초기화
        return false;
      }
      return true;
    }
    
    function preview(arr){
      arr.forEach(function(f){
        
        //파일명이 길면 파일명...으로 처리
        var fileName = f.name;
        if(fileName.length > 10){
          fileName = fileName.substring(0,7)+"...";
        }
        
        //div에 이미지 추가
        var str = '<div style="display: inline-flex; padding: 10px;"><li>';
        str += '<span>'+fileName+'</span><br>';
        
        //이미지 파일 미리보기
        if(f.type.match('image.*')){
          var reader = new FileReader(); //파일을 읽기 위한 FileReader객체 생성
          reader.onload = function (e) { //파일 읽어들이기를 성공했을때 호출되는 이벤트 핸들러
            //str += '<button type="button" class="delBtn" value="'+f.name+'" style="background: red">x</button><br>';
            str += '<img class = "ui fluid image" src="'+e.target.result+'" title="'+f.name+'"/>';
            str += '</li></div>';
            $(str).appendTo('#preview');
          } 
          reader.readAsDataURL(f);
        }else{
          str += '<img src="/resources/img/fileImg.png" title="'+f.name+'" width=300 height=300 />';
          $(str).appendTo('#preview');
        }
      });//arr.forEach
    }
  });
</script>
</html>
