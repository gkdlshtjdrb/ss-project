package kr.co.sshero.ssquest;

import java.util.List;

import kr.co.sshero.quest.QuestVO;
import kr.co.sshero.user.UserVO;


public interface SSQuestService {
	void insertSSQuest(SSQuestVO vo);

	void updateSSQuest(SSQuestVO vo);

	void deleteSSQuest(SSQuestVO vo);

	SSQuestVO getMyQuest(SSQuestVO vo);

	List<SSQuestVO> getMyQuestList(UserVO vo);
	
	void questReward(SSQuestVO vo);
	void deleteFile(QuestFileVO vo);
	void rcheckUpdate(SSQuestVO vo);
	

}
