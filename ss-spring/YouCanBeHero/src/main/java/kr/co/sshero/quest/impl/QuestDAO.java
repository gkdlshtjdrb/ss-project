package kr.co.sshero.quest.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import kr.co.sshero.quest.QuestVO;

@Repository("questDAO")
public class QuestDAO {
	
	@Autowired
	private SqlSessionTemplate mybatis;
	
	
	public void insertQuset(QuestVO vo) {
		mybatis.insert("QuestDAO.insertQuest", vo);
	}
	
	public void deleteQuest(QuestVO vo) {
		mybatis.update("QuestDAO.deleteQuest", vo);
	}
	
	public QuestVO getQuest(QuestVO vo) {
		
		try {

			return mybatis.selectOne("QuestDAO.getQuest", vo);

		} catch (EmptyResultDataAccessException e) {

			return null;

		}

	
	}
	
	
	public void updateQuest(QuestVO vo) {
		
		mybatis.update("QuestDAO.updateQuest", vo);
	}
	
	public List<QuestVO> getDdayQuestList() {
		return mybatis.selectList("QuestDAO.getDdayQuestList");
	}
	
	public List<QuestVO> getWeekQuestList() {
		return mybatis.selectList("QuestDAO.getWeekQuestList");
	}
	
	public List<QuestVO> getSubmitList(QuestVO vo){
		System.out.println("dao확인"+ vo.getQid());
		return mybatis.selectList("QuestDAO.getSubmitList", vo);

	}
}
