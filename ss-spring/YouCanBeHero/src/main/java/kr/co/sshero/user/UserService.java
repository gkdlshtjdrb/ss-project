package kr.co.sshero.user;

public interface UserService {
	UserVO getUser(UserVO vo);
	
	UserVO signUp(UserVO vo);
}
