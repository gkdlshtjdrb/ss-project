<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
        <link href="<c:url value="/resources/semantic/semantic.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/semantic/semantic.js" />"></script>
    <title>Document</title>


</head>
<body>
    <div class="ui secondary pointing menu">
        <a name = "week" class="item" href="getCustomWeekQuestList.do">
          	주간퀘스트
        </a>
        <a name = "dday" class="item" href="getCustomDdayQuestList.do">
          	일일퀘스트
        </a>
        <a class="item">
          My Team List
        </a>
        <a class="ui item" href = "insertQuest.do">
            Insert
          </a>
          <a class="ui item">
            Logout
          </a>
        
      </div>
      <div class="ui segment">
      <form action = "updateQuest.do" method = "post">
      <input name="qid" type="hidden" value="${quest.qid}"/>
        <div class="ui form">
          <div class="field">
            <label>제목</label>
            <textarea rows="1" name = "qname">${quest.qname }</textarea>
          </div>

          <div class="field">
            <label>내용</label>
            <textarea name = "qcontext">${quest.qcontext }</textarea>
          </div>

          <div class="fields">
            <div class="field">
              <label>경험치</label>
              <input type="number" name = "qexp" placeholder="qexp" value = "${quest.qexp }">
            </div>
            <div class="field">
              <label>코인</label>
              <input type="number" name = "qcoin" placeholder="coin" value = "${quest.qcoin }">
            </div>
          </div>


          <label>기간</label>
          <fmt:formatDate value="${quest.startdate }" pattern="yyyy-MM-dd" var="startDate"/> 
		<fmt:formatDate value="${quest.enddate }" pattern="yyyy-MM-dd" var="endDate"/>   
          <div class="inline fields">
            <div class="eight wide field">
              <label>시작</label>
              <input type="text" name = "startdateString" placeholder="예)YYYY-MM-DD" value = "${startDate }">
            </div>

            <div class="eight wide field">
              <label>끝</label>
              <input type="text" name = "enddateString" placeholder="예)YYYY-MM-DD" value = "${endDate }">
            </div>
          </div>
          
			<div class = "inline fields">
            <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="weekcheck" checked="" tabindex="0" value = "1">
                <label>주 1회</label>
              </div>
            </div>
            <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="weekcheck" tabindex="0" value = "0">
                <label>매일</label>
              </div>
            </div>
          </div>

          <button class="ui primary submit button" type = "submit">
          		  수정
          </button>
          <a class="ui button" href = "deleteQuest.do?qid=${quest.qid }">
            	삭제
          </a>
        </div>
         </form>
      </div>
      
      <script>
		var type = ${quest.weekcheck}
		if( type == 0){
			$("a[name= dday]").addClass("active");
		}
		else if(type == 1) {
			$("a[name= week]").addClass("active");
		}
	
		</script>
</body>
</html>
