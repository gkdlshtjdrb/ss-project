package kr.co.sshero.user.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import kr.co.sshero.user.UserVO;

@Repository("userDAO")
public class UserDAO {

	@Autowired
	private SqlSessionTemplate mybatis;
	
	public UserVO getUser(UserVO vo) {
		System.out.println(mybatis.getConnection());
		try {
			return mybatis.selectOne("LoginDAO.getUser", vo);
		} catch(DataAccessException e) {
			return null;
		}
	
		
	}
	
	public void singUp(UserVO vo) {
		mybatis.selectOne("LoginDAO.singUp", vo);
		
	}
	
	public int idcheck(UserVO vo) {
		return mybatis.selectOne("LoginDAO.idCheck", vo);
	
		
	}
}
