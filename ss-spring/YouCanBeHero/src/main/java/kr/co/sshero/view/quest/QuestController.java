package kr.co.sshero.view.quest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import kr.co.sshero.fileIO.FileIO;
import kr.co.sshero.quest.QuestService;
import kr.co.sshero.quest.QuestVO;
import kr.co.sshero.quest.impl.QuestDAO;
import kr.co.sshero.ssquest.QuestFileVO;
import kr.co.sshero.ssquest.SSQuestService;
import kr.co.sshero.ssquest.SSQuestVO;
import kr.co.sshero.user.UserVO;

@Controller
@SessionAttributes("user")
public class QuestController {
	
	@Autowired
	private QuestService questService;
	
	private static final String UPLOAD_PATH = "D:\\programing\\fileuploadtest\\";
	
	@RequestMapping("getCustomWeekQuestList.do")
	public String getCustomWeekQuestList(Model model){
		model.addAttribute("questList",questService.getWeekQuestList());
		
		model.addAttribute("mapping", 0);
		return "AdminViews/QuestList";
		
	}
	
	@RequestMapping("getCustomDdayQuestList.do")
	public String getCustomDdayQuestList(Model model){
		model.addAttribute("questList",questService.getDdayQuestList());
		model.addAttribute("mapping", 1);
		return "AdminViews/QuestList";
		
	}
	
	@RequestMapping("getQuest.do")
	public String getQuest(QuestVO vo, Model model) {
	
		QuestVO quest = new QuestVO();
		quest = questService.getQuest(vo);
		if(quest != null) {
			model.addAttribute("quest", quest);
			return "AdminViews/getQuest";
		} else {
			return("redirect:getCustomWeekQuestList.do");
		}
		
	}
	
	@RequestMapping("updateQuest.do")
	public String updateQuest(QuestVO vo) {
		questService.updateQuest(vo);
		
		return("redirect:getCustomWeekQuestList.do");
	}
	
	@RequestMapping(value = "insertQuest.do", method = RequestMethod.GET)
	public String insertQuestView() {
		return "AdminViews/insertQuest";
	}
	
	@RequestMapping(value = "insertQuest.do", method = RequestMethod.POST)
	public String insertQuest(QuestVO vo) {
		questService.insertQuest(vo);
		return("redirect:getCustomWeekQuestList.do");
	}
	
	@RequestMapping("deleteQuest.do")
	public String deleteQuest(QuestVO vo) {

		questService.deleteQuest(vo);
		return("redirect:getCustomWeekQuestList.do");
	}
	
	
//	@RequestMapping("questSubmitList.do")
//	public String questSubmitList(QuestVO vo, Model model, @RequestParam(value = "qid") int qid) {
//		System.out.println("qid =="+ qid);
//		vo.setQid(qid);
//		model.addAttribute("submitList", questService.getSubmitList(vo));
//
//		return "AdminViews/submitList";
//	}
	
	@RequestMapping("questSubmitList.do")
	public String questSubmitList(QuestVO vo, Model model, @RequestParam(value = "rqid",required =false, defaultValue="-1") int rqid) {
		if(rqid == -1) {
			model.addAttribute("submitList", questService.getSubmitList(vo));
		}else {
			vo.setQid(rqid);
			model.addAttribute("submitList", questService.getSubmitList(vo));
		}

		return "AdminViews/submitList";
	}
	
	@RequestMapping("fileDownload.do")
	public ModelAndView fileDownload(@RequestParam(value = "filepath") String filepath) {
		System.out.println("filepath" + filepath);
	
        File downloadFile = new File(UPLOAD_PATH + filepath);
 
        // 생성된 객체 파일과 view들을 인자로 넣어 새 ModelAndView 객체를 생성하며 파일을 다운로드
        // (자동 rendering 해줌)
        return new ModelAndView("fileDownloadView", "downloadFile", downloadFile);
	}
	
	//==============================ss들이 사용하는 기능=======================================
	@RequestMapping("getDdayQuestList.do")
	public String getDdayQuestList(Model model, @ModelAttribute("user") UserVO vo) {
		model.addAttribute("questList",questService.getDdayQuestList(vo));
		model.addAttribute("mapping", 1);
		return "SSviews/SSQuestList";
	}
	
	
	@RequestMapping("getWeekQuestList.do")
	public String getWeekQuestList(Model model, @ModelAttribute("user") UserVO vo) {
		model.addAttribute("questList",questService.getWeekQuestList(vo));
		model.addAttribute("mapping", 0);
		return "SSviews/SSQuestList";
	}
	

	

	
}
