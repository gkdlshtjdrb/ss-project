package kr.co.sshero.fileIO;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import kr.co.sshero.ssquest.QuestFileVO;
import kr.co.sshero.ssquest.SSQuestVO;

@Component
public class FileIO {
	
	private static final String UPLOAD_PATH = "D:\\programing\\fileuploadtest";
	
	public List<QuestFileVO> fileLoad(String files, List<QuestFileVO> filess){
		String[] filepath = files.split(",");
		String[] filename = new String[filepath.length];
		QuestFileVO[] fvo = new QuestFileVO[filepath.length];
		for(int i = 0; i < filepath.length; i++) {
			
			filename[i] = filepath[i].substring(37);
			fvo[i] = new QuestFileVO();
			fvo[i].setFilepath(filepath[i]);
			fvo[i].setFilename(filename[i]);
			filess.add(fvo[i]);
			
			System.out.println("fvo에 들어가는 파일 이름= "+fvo[i].getFilename()+" 파일 경로 = "+fvo[i].getFilepath());
		}	
		
		return filess;
	}
	
	public String saveFile(MultipartFile file) {
		UUID uuid = UUID.randomUUID();
		String saveName = uuid + "_" + file.getOriginalFilename();
		

		File saveFile = new File(UPLOAD_PATH,saveName);
		try {
	        file.transferTo(saveFile); 
	    } catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
		return saveName;
	}
	
	public SSQuestVO insertFile(SSQuestVO vo, MultipartFile[] files) {
		
		String savename = "";
		
		if(files[0].getOriginalFilename() != "") {
			for(MultipartFile f: files) {
				savename += saveFile(f); 
				savename += ",";
			}
			vo.setRfile(savename);
		}else {vo.setRfile("");}
		
		return vo;
	}
	
public SSQuestVO insertFile(SSQuestVO vo, MultipartFile[] files, String savename) {
		
	if(files[0].getOriginalFilename() != "") {
		for(MultipartFile f: files) {
			savename += saveFile(f); 
			savename += ",";
			
			System.out.println("save파일 실행 확인"+savename);
		}
		
		vo.setRfile(savename);
	}else {vo.setRfile(savename);}
	
	return vo;
	}

}
