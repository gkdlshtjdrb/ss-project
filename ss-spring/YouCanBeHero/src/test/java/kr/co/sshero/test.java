package kr.co.sshero;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class test {

	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
//		startdate는 리턴되는 요일 정숫값 -1을 sysdate에서 빼주면 됨.
//		enddate는 리턴되는 9 - 요일 정숫값을 sysdate에 더해주면 됨.
		cal.setTime(new Date());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("오늘 날짜" + df.format(cal.getTime()));
 
        
//		cal.add(Calendar.DATE, -(dayOfWeek-1));
//		System.out.println("after: " + df.format(cal.getTime()));
		
		cal.add(Calendar.DATE, (9-dayOfWeek));
		System.out.println("after: " + df.format(cal.getTime()));


	}

}
