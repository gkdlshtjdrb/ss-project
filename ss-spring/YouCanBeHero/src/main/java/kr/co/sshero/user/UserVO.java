package kr.co.sshero.user;

public class UserVO {

	private String ssid;
	private String sspassword;
	private String ssname;
	private int ssexp;
	private int sslv;
	private int admin;
	

	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	public String getSspassword() {
		return sspassword;
	}
	public void setSspassword(String sspassword) {
		this.sspassword = sspassword;
	}
	public String getSsname() {
		return ssname;
	}
	public void setSsname(String ssname) {
		this.ssname = ssname;
	}
	public int getSsexp() {
		return ssexp;
	}
	public void setSsexp(int ssexp) {
		this.ssexp = ssexp;
	}
	public int getSslv() {
		return sslv;
	}
	public void setSslv(int sslv) {
		this.sslv = sslv;
	}
	public int getAdmin() {
		return admin;
	}
	public void setAdmin(int admin) {
		this.admin = admin;
	}
	
	
	
}
